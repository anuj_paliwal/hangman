class Hangman
	MAX_WRONG_GUESSES = 12
	attr_accessor :guesser, :chooser

	def initialize
		set_mode
		play
	end

	def set_mode
		modes = "Choose mode: \n1. Human guesses computer generated word\n"
		modes << "2. Computer guesses human generated word\n"
		modes << "3. Computer vs Computer\n4. Human vs Human"
		
		puts modes
		mode = gets.chomp.to_i
		until (1).upto(4).include?(mode)
			puts "\nInvalid mode choice. Please enter a valid choice.\n#{modes}\n"
			mode = gets.chomp.to_i
		end
		case mode 
		when 1
			self.guesser = HumanPlayer.new
			self.chooser = ComputerPlayer.new
		when 2
			self.guesser = ComputerPlayer.new
			self.chooser = HumanPlayer.new
		when 3
			self.guesser = ComputerPlayer.new
			self.chooser = ComputerPlayer.new
		when 4
			self.chooser = HumanPlayer.new
			self.guesser = HumanPlayer.new
		end
	end


	def play
		chooser.pick_word
		wrong_guesses = 0

		until wrong_guesses == MAX_WRONG_GUESSES
			guess = guesser.guess_letter(chooser.exposed)
			if chooser.guess_correct?(guess)
				chooser.update_exposed(guess)
			else
				wrong_guesses += 1
			end
			if chooser.confirm_victory?
				puts "\n\n\n\nGuesser wins, the secret word is #{self.chooser.exposed}\n\n\n\n"
				return
			end
		end

		puts "You Suck!"
	end
end

class HumanPlayer
	attr_accessor :exposed, :secret_word_length, :exposed_indices

	def initialize
		@exposed_indices = []
		@secret_word_length = 0
	end
	
	def guess_letter(exposed_word)
		puts exposed_word
		puts "Pick a letter"
		guess = gets.chomp.downcase
		unless ('a'..'z').to_a.include?(guess)
			puts "That is not a letter. Please enter an actual letter.\n"
			guess = gets.chomp.downcase
		end
	end

	def pick_word
		puts "Think of word and input it's length"
		self.secret_word_length = gets.chomp
		unless secret_word_length.to_i != 0
			puts "That was not a number. Please enter a number"
			self.secret_word_length = gets.chomp
		end
		self.exposed = "_" * secret_word_length.to_i
	end

	def update_exposed(correct_guess)
		puts "Enter positions of correct guesses separated by commas"
		indices_str = gets.chomp
		index_range = (0..secret_word_length.to_i - 1).to_a
		until index_range.include?(indices_str)
			puts "Outside of index range, enter valid index."
		 	indices_str = gets.chomp
		end
		guessed_letter_indices = indices_str.split(',').map {|index| index.to_i}
		self.exposed_indices << guessed_letter_indices
		guessed_letter_indices.each do |letter_index|
			self.exposed[letter_index] = correct_guess
		end
	end

	def guess_correct?(guess)
		puts "Does your word contain the letter #{guess}?\nyes or no?"
		answer = gets.chomp
		if answer == 'yes'
			return true 
		elsif answer == 'no'
			return false
		else
			puts "Please enter 'yes' or 'no'"
			guess_correct?(guess)
		end
	end

	def confirm_victory?
		true unless self.exposed.include?('_')
	end
end


class ComputerPlayer
	attr_accessor :secret_word, :exposed, :already_guessed

	def initialize
		@already_guessed = []
	end

	def pick_word
		dictionary = File.readlines("dictionary.txt").map do |line|
			word = line.chomp
		end
		self.secret_word = dictionary.sample
		self.exposed = "_" * self.secret_word.length
	end

	def guess_letter(exposed_word)
		next_guess = ''
		puts exposed_word
		dictionary = File.readlines("dictionary.txt").map do |line|
			line.chomp
		end
		same_length_words = dictionary.select {|word| word.length == exposed_word.length}
		exposed_indices = exposed_letters_hash(exposed_word)
		worthy_candidates = same_length_words.select do |word|
			worthy = true
			exposed_indices.each do |index, letter|
				worthy = false unless word[index] == letter
			end
			worthy
		end
		most_frequent_letter = Hash.new(0)
		worthy_candidates.each do |word|
			word.split('').each do |letter|
				unless exposed_word.include?(letter) || self.already_guessed.include?(letter)
					most_frequent_letter[letter] += 1 
				end
			end
		end

		next_guess = most_frequent_letter.max_by{ |letter, freq| freq }[0]

		self.already_guessed << next_guess
		next_guess
	end

	def exposed_letters_hash(exposed_word)
		exposed_letter_indices = {}
		exposed_word.split('').each_with_index do |exposed_letter, idx|
		exposed_letter_indices[idx] = exposed_letter unless exposed_letter == '_'
		end

		exposed_letter_indices
	end

	def guess_correct?(guess)
		self.secret_word.include?(guess)
	end

	def update_exposed(correct_guess)
		guessed_letter_indices = []
		
		
		self.secret_word.split('').each_with_index do |letter, idx|
			guessed_letter_indices << idx if correct_guess == letter
		end

		guessed_letter_indices.each do |letter_index|
			self.exposed[letter_index] = correct_guess
		end
	end

	def confirm_victory?
		self.exposed == self.secret_word
	end
end
